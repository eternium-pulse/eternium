<?php

return (new \PhpCsFixer\Config())
    ->setRules([
        '@PhpCsFixer' => true,
    ])
    ->setFinder(
        (new \PhpCsFixer\Finder())
            ->files()
            ->in(['src', 'tests'])
            ->append([basename(__FILE__)])
    )
;

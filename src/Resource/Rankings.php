<?php

declare(strict_types=1);

namespace EterniumPulse\Resource;

use Symfony\Contracts\HttpClient\HttpClientInterface;

/**
 * @implements \IteratorAggregate<int, array<string, mixed>>
 */
final class Rankings implements \IteratorAggregate, \Stringable
{
    public function __construct(
        private HttpClientInterface $client,
        private string $id,
        private string $subleaderboard = '',
    ) {
        assert(24 === strlen($id) && ctype_xdigit($id), 'leaderboard ID is valid');
    }

    /**
     * @codeCoverageIgnore
     */
    private function __clone()
    {
    }

    public function __toString(): string
    {
        return "leaderboards/{$this->id}/rankings";
    }

    /**
     * @return \Iterator<int, array<string, mixed>>
     */
    public function getIterator(): \Iterator
    {
        $options = [
            'page' => 1,
            'page_size' => 100,
        ];

        do {
            $entries = $this->list($options);
            foreach ($entries as $entry) {
                yield $entry;
            }
            ++$options['page'];
        } while (count($entries) === $options['page_size']);
    }

    /**
     * @param array{page?: int, page_size?: int, payload?: array<string>|string, show_challenger?: bool} $options
     *
     * @return array<int, array<string, mixed>>
     */
    public function list(array $options = []): array
    {
        return $this->client->request('GET', "{$this}", [
            'query' => $this->makeQuery(...$options),
        ])->toArray();
    }

    /**
     * @param array{payload?: array<string>|string, show_challenger?: bool} $options
     *
     * @return array<string, mixed>
     */
    public function getPlayer(string $challenger, array $options = []): array
    {
        return $this->client->request('GET', "{$this}/{$challenger}", [
            'query' => $this->makeQuery(...$options),
        ])->toArray();
    }

    /**
     * @param array{payload?: array<string>|string, show_challenger?: bool} $options
     *
     * @return array<string, mixed>
     */
    public function getAt(int $rank, array $options = []): array
    {
        assert($rank > 0, 'rank is positive');

        return $this->client->request('GET', "{$this}/{$rank}", [
            'query' => $this->makeQuery(...$options),
        ])->toArray();
    }

    /**
     * @param array<string>|string $payload
     *
     * @return array<string, string>
     */
    private function makeQuery(
        int $page = 0,
        int $page_size = 0,
        array|string $payload = '',
        bool $show_challenger = false,
    ): array {
        assert($page >= 0, 'page is non-negative');
        assert($page_size >= 0, 'page size is non-negative');

        $query = [];
        if ($page > 0) {
            $query['page'] = (string) $page;
        }
        if ($page_size > 0) {
            $query['pageSize'] = (string) $page_size;
        }
        if (is_array($payload)) {
            $payload = join(',', $payload);
        }
        if ('' !== $payload) {
            $query['payload'] = $payload;
        }
        if ($show_challenger) {
            $query['show_challenger'] = 'true';
        }
        if ('' !== $this->subleaderboard) {
            $query['subleaderboard'] = $this->subleaderboard;
        }

        return $query;
    }
}

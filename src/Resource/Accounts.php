<?php

declare(strict_types=1);

namespace EterniumPulse\Resource;

use Symfony\Contracts\HttpClient\HttpClientInterface;

final class Accounts implements \Stringable
{
    public function __construct(
        private HttpClientInterface $client,
    ) {
    }

    /**
     * @codeCoverageIgnore
     */
    private function __clone()
    {
    }

    public function __toString(): string
    {
        return 'accounts';
    }

    /**
     * @return array<string, mixed>
     */
    public function get(string $email): array
    {
        assert(false !== filter_var($email, FILTER_VALIDATE_EMAIL), 'email is valid');

        return $this->client->request('GET', "{$this}/{$email}")->toArray();
    }
}

<?php

declare(strict_types=1);

namespace EterniumPulse\Resource;

use Symfony\Contracts\HttpClient\HttpClientInterface;

/**
 * @implements \IteratorAggregate<int, array<string, mixed>>
 */
final class Leaderboards implements \IteratorAggregate, \Stringable
{
    public function __construct(
        private HttpClientInterface $client,
    ) {
    }

    /**
     * @codeCoverageIgnore
     */
    private function __clone()
    {
    }

    public function __toString(): string
    {
        return 'leaderboards';
    }

    /**
     * @return \Iterator<int, array<string, mixed>>
     */
    public function getIterator(): \Iterator
    {
        yield from $this->list();
    }

    /**
     * @return array<int, array<string, mixed>>
     */
    public function list(): array
    {
        return $this->client->request('GET', "{$this}")->toArray();
    }

    /**
     * @return array<string, mixed>
     */
    public function get(string $id): array
    {
        assert(24 === strlen($id) && ctype_xdigit($id), 'leaderboard ID is valid');

        return $this->client->request('GET', "{$this}/{$id}")->toArray();
    }

    public function getRankings(string $id, string $subleaderboard = ''): Rankings
    {
        return new Rankings($this->client, $id, $subleaderboard);
    }
}

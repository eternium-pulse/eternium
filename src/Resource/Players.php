<?php

declare(strict_types=1);

namespace EterniumPulse\Resource;

use Symfony\Contracts\HttpClient\HttpClientInterface;

final class Players implements \Stringable
{
    public function __construct(
        private HttpClientInterface $client,
    ) {
    }

    /**
     * @codeCoverageIgnore
     */
    private function __clone()
    {
    }

    public function __toString(): string
    {
        return 'players';
    }

    /**
     * @return array<int, array<string, mixed>>
     */
    public function list(string $query): array
    {
        assert('' !== $query);

        $options = [];
        if (str_starts_with($query, 'MMID-')) {
            $options['provider'] = 'legacy';
            $options['principal'] = $query;
        } elseif (str_contains($query, '@')) {
            $options['provider'] = 'email';
            $options['principal'] = $query;
        } else {
            $options['username'] = $query;
        }

        return $this->client->request('GET', "{$this}", [
            'query' => $options,
        ])->toArray();
    }

    /**
     * @return array<string, mixed>
     */
    public function get(string $id): array
    {
        assert(24 === strlen($id) && ctype_xdigit($id), 'player ID is valid');

        return $this->client->request('GET', "{$this}/{$id}")->toArray();
    }

    /**
     * @return array<int, array<string, mixed>>
     */
    public function getAuthTokens(string $id): array
    {
        assert(24 === strlen($id) && ctype_xdigit($id), 'player ID is valid');

        return $this->client->request('GET', "{$this}/{$id}/authTokens")->toArray(false);
    }
}

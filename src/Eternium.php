<?php

declare(strict_types=1);

namespace EterniumPulse;

use Symfony\Component\HttpClient\HttpClient;
use Symfony\Contracts\HttpClient\HttpClientInterface;

final class Eternium
{
    public const BASE_URI = 'https://mfp.makingfun.com/api/';

    public Resource\Accounts $accounts;
    public Resource\Leaderboards $leaderboards;
    public Resource\Players $players;

    public function __construct(HttpClientInterface $client)
    {
        $this->accounts = new Resource\Accounts($client);
        $this->leaderboards = new Resource\Leaderboards($client);
        $this->players = new Resource\Players($client);
    }

    /**
     * @codeCoverageIgnore
     */
    private function __clone()
    {
    }

    /**
     * @return array<string, mixed>
     */
    public static function getDefaultOptions(string $apiKey): array
    {
        assert(64 == strlen($apiKey) && ctype_xdigit($apiKey), 'API key is valid');

        return [
            'base_uri' => self::BASE_URI,
            'http_version' => '1.1',
            'max_redirects' => 0,
            'headers' => [
                'Accept' => 'application/json',
                'X-API-Key' => $apiKey,
            ],
        ];
    }

    /**
     * @codeCoverageIgnore
     */
    public static function createDefault(string $apiKey): self
    {
        assert(class_exists(HttpClient::class), 'symfony/http-client installed');

        return new self(HttpClient::createForBaseUri(
            self::BASE_URI,
            self::getDefaultOptions($apiKey),
        ));
    }
}

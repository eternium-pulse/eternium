<?php

declare(strict_types=1);

namespace EterniumPulse\Resource;

use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\TestCase;
use Symfony\Contracts\HttpClient\HttpClientInterface;

/**
 * @internal
 *
 * @covers \EterniumPulse\Resource\Leaderboards
 */
final class LeaderboardsTest extends TestCase
{
    /**
     * @var HttpClientInterface&MockObject
     */
    private HttpClientInterface $client;

    protected function setUp(): void
    {
        $this->client = $this->createMock(HttpClientInterface::class);
    }

    public function testToString(): void
    {
        $this->assertEquals('leaderboards', new Leaderboards($this->client));
    }

    public function testGetIterator(): void
    {
        $this->client
            ->expects($this->once())
            ->method('request')
            ->with('GET', 'leaderboards')
        ;

        foreach (new Leaderboards($this->client) as $_);
    }

    public function testList(): void
    {
        $this->client
            ->expects($this->once())
            ->method('request')
            ->with('GET', 'leaderboards')
        ;

        (new Leaderboards($this->client))->list();
    }

    /**
     * @testWith ["000000000000000000000000"]
     */
    public function testGet(string $id): void
    {
        $this->client
            ->expects($this->once())
            ->method('request')
            ->with('GET', "leaderboards/{$id}")
        ;

        (new Leaderboards($this->client))->get($id);
    }

    /**
     * @doesNotPerformAssertions
     *
     * @testWith ["000000000000000000000000", ""]
     *           ["000000000000000000000000", "NONE"]
     */
    public function testGetRankings(string $id, string $sub): void
    {
        (new Leaderboards($this->client))->getRankings($id, $sub);
    }
}

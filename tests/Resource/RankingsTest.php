<?php

namespace EterniumPulse\Resource;

use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\TestCase;
use Symfony\Contracts\HttpClient\HttpClientInterface;

/**
 * @internal
 *
 * @covers \EterniumPulse\Resource\Rankings
 */
final class RankingsTest extends TestCase
{
    /**
     * @var HttpClientInterface&MockObject
     */
    private HttpClientInterface $client;

    protected function setUp(): void
    {
        $this->client = $this->createMock(HttpClientInterface::class);
    }

    /**
     * @testWith ["000000000000000000000000", ""]
     *           ["000000000000000000000000", "NONE"]
     */
    public function testToString(string $id, string $sub): void
    {
        $this->assertEquals("leaderboards/{$id}/rankings", new Rankings($this->client, $id, $sub));
    }

    /**
     * @testWith ["000000000000000000000000", ""]
     *           ["000000000000000000000000", "NONE"]
     */
    public function testGetIterator(string $id, string $sub): void
    {
        $this->client
            ->expects($this->atLeastOnce())
            ->method('request')
            ->with('GET', "leaderboards/{$id}/rankings")
        ;

        foreach (new Rankings($this->client, $id, $sub) as $_);
    }

    /**
     * @testWith ["000000000000000000000000", ""]
     *           ["000000000000000000000000", "NONE"]
     */
    public function testList(string $id, string $sub): void
    {
        $this->client
            ->expects($this->atLeastOnce())
            ->method('request')
            ->with('GET', "leaderboards/{$id}/rankings")
        ;

        (new Rankings($this->client, $id, $sub))->list();
    }

    /**
     * @testWith ["000000000000000000000000", "", "0$HERO"]
     *           ["000000000000000000000000", "NONE", "0$HERO"]
     */
    public function testGetPlayer(string $id, string $sub, string $challenger): void
    {
        $this->client
            ->expects($this->once())
            ->method('request')
            ->with('GET', "leaderboards/{$id}/rankings/{$challenger}")
        ;

        (new Rankings($this->client, $id, $sub))->getPlayer($challenger);
    }

    /**
     * @testWith ["000000000000000000000000", "", 1]
     *           ["000000000000000000000000", "NONE", 2]
     */
    public function testGetAt(string $id, string $sub, int $rank): void
    {
        $this->client
            ->expects($this->once())
            ->method('request')
            ->with('GET', "leaderboards/{$id}/rankings/{$rank}")
        ;

        (new Rankings($this->client, $id, $sub))->getAt($rank);
    }
}

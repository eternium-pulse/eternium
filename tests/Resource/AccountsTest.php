<?php

declare(strict_types=1);

namespace EterniumPulse\Resource;

use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\TestCase;
use Symfony\Contracts\HttpClient\HttpClientInterface;

/**
 * @internal
 *
 * @covers \EterniumPulse\Resource\Accounts
 */
final class AccountsTest extends TestCase
{
    /**
     * @var HttpClientInterface&MockObject
     */
    private HttpClientInterface $client;

    protected function setUp(): void
    {
        $this->client = $this->createMock(HttpClientInterface::class);
    }

    public function testToString(): void
    {
        $accounts = new Accounts($this->client);

        $this->assertEquals('accounts', $accounts->__toString());
    }

    /**
     * @testWith ["nobody@example.com"]
     */
    public function testGet(string $email): void
    {
        $this->client
            ->expects($this->once())
            ->method('request')
            ->with('GET', "accounts/{$email}")
        ;

        $accounts = new Accounts($this->client);
        $accounts->get($email);
    }
}

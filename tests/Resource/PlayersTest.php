<?php

declare(strict_types=1);

namespace EterniumPulse\Resource;

use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\TestCase;
use Symfony\Contracts\HttpClient\HttpClientInterface;

/**
 * @internal
 *
 * @covers \EterniumPulse\Resource\Players
 */
final class PlayersTest extends TestCase
{
    /**
     * @var HttpClientInterface&MockObject
     */
    private HttpClientInterface $client;

    protected function setUp(): void
    {
        $this->client = $this->createMock(HttpClientInterface::class);
    }

    public function testToString(): void
    {
        $players = new Players($this->client);

        $this->assertSame('players', $players->__toString());
    }

    /**
     * @testWith ["legacy", "MMID-00000000-00000000-00000000-00000000000000000000000000000000"]
     *           ["email", "nobody@example.com"]
     */
    public function testListByProvider(string $provider, string $principal): void
    {
        $this->client
            ->expects($this->once())
            ->method('request')
            ->with('GET', 'players', ['query' => [
                'provider' => $provider,
                'principal' => $principal,
            ]])
        ;

        (new Players($this->client))->list($principal);
    }

    /**
     * @testWith ["Noname"]
     */
    public function testListByUsername(string $username): void
    {
        $this->client
            ->expects($this->once())
            ->method('request')
            ->with('GET', 'players', ['query' => [
                'username' => $username,
            ]])
        ;

        (new Players($this->client))->list($username);
    }

    /**
     * @testWith ["000000000000000000000000"]
     */
    public function testGet(string $id): void
    {
        $this->client
            ->expects($this->once())
            ->method('request')
            ->with('GET', "players/{$id}")
        ;

        (new Players($this->client))->get($id);
    }

    /**
     * @testWith ["000000000000000000000000"]
     */
    public function testGetAuthTokens(string $id): void
    {
        $this->client
            ->expects($this->once())
            ->method('request')
            ->with('GET', "players/{$id}/authTokens")
        ;

        (new Players($this->client))->getAuthTokens($id);
    }
}

<?php

declare(strict_types=1);

namespace EterniumPulse;

use PHPUnit\Framework\TestCase;
use Symfony\Contracts\HttpClient\HttpClientInterface;

/**
 * @internal
 *
 * @covers \EterniumPulse\Eternium
 */
final class EterniumTest extends TestCase
{
    public function testResources(): void
    {
        $eternium = new Eternium($this->createMock(HttpClientInterface::class));

        $this->assertObjectHasAttribute('accounts', $eternium);
        $this->assertObjectHasAttribute('leaderboards', $eternium);
        $this->assertObjectHasAttribute('players', $eternium);
    }

    /**
     * @testWith ["0000000000000000000000000000000000000000000000000000000000000000"]
     */
    public function testGetDefaultOptions(string $apiKey): void
    {
        $options = Eternium::getDefaultOptions($apiKey);

        $this->assertArrayHasKey('base_uri', $options);
        $this->assertSame('https://mfp.makingfun.com/api/', $options['base_uri']);

        $this->assertArrayHasKey('headers', $options);
        $this->assertIsArray($options['headers']);
        $this->assertArrayHasKey('X-API-Key', $options['headers']);
        $this->assertSame($apiKey, $options['headers']['X-API-Key']);
    }
}
